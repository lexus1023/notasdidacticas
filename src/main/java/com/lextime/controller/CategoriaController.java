package com.lextime.controller;

import com.lextime.ejb.CategoriaFacadeLocal;
import com.lextime.model.Categoria;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class CategoriaController implements Serializable{

    @EJB//para evitar la instancia de Categoria ya que las interfacesno se instancian
    private CategoriaFacadeLocal categoriaEJB;
    private Categoria categoria; //variable para guardar los objetos

    @PostConstruct
    public void init() {
        categoria = new Categoria();//se puede hacer uso de Spring para hacer inyecciones como arriba con EJB
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public void registrar() {
            categoriaEJB.create(categoria);
            System.out.println("hemos hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh");
    }

    
}
