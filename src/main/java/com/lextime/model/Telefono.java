package com.lextime.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="telefono")
public class Telefono implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int codigo_telefono;
    
    @Column(name = "numero")
    private String numero;
    
    @ManyToOne
    @JoinColumn(name = "codigo_persona")
    private Persona codigo_persona;
    

    public int getCodigo_telefono() {
        return codigo_telefono;
    }

    public void setCodigo_telefono(int codigo_telefono) {
        this.codigo_telefono = codigo_telefono;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Persona getCodigo_persona() {
        return codigo_persona;
    }

    public void setCodigo_persona(Persona codigo_persona) {
        this.codigo_persona = codigo_persona;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + this.codigo_telefono;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Telefono other = (Telefono) obj;
        if (this.codigo_telefono != other.codigo_telefono) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Telefono{" + "codigo_telefono=" + codigo_telefono + '}';
    }



}
