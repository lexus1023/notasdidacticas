
package com.lextime.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
public class Usuario implements Serializable{
   @Id
   @OneToOne(cascade = CascadeType.PERSIST)
   @JoinColumn(name = "codigo_persona", nullable= false)
    private Persona codigo_usuario;
   @Column(name="usuario_ci")
    private String usuario_ci;
   @Column(name="clave")
   private String clave;
   @Column(name="tipo")
   private String tipo;
   @Column(name="estado")
   private short estado=1;

    public Persona getCodigo_usuario() {
        return codigo_usuario;
    }

    public void setCodigo_usuario(Persona codigo_usuario) {
        this.codigo_usuario = codigo_usuario;
    }

    public String getUsuario_ci() {
        return usuario_ci;
    }

    public void setUsuario_ci(String usuario_ci) {
        this.usuario_ci = usuario_ci;
    }

   

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public short getEstado() {
        return estado;
    }

    public void setEstado(short estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.codigo_usuario);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (!Objects.equals(this.codigo_usuario, other.codigo_usuario)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Usuario{" + "codigo_usuario=" + codigo_usuario + '}';
    }

    
    
    
}

