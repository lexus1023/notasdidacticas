/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lextime.ejb;

import com.lextime.model.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Alexis
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> implements UsuarioFacadeLocal {

    @PersistenceContext(unitName = "notesprime")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }

    @Override
    public Usuario iniciarSesion(Usuario us) {
        Usuario usuario = null;
        String consulta;
        try {
            consulta = "FROM Usuario u WHERE u.usuario_ci=?1 and u.clave =?2";
            Query query = em.createQuery(consulta);
            query.setParameter(1, us.getUsuario_ci());
            query.setParameter(2, us.getClave());
            List<Usuario> lista = query.getResultList();
            if (!lista.isEmpty()) {//si la lista es diferente de vacia es deicr hay una aunque sea
                usuario = lista.get(0);//se devuelve el primero de la lista
            }

        } catch (Exception e) {
            throw e;
        }
        //finally{
        //em.close(); se usa solo si se esta usando un framework como Spring si no no ..(en EJb)
        //}
        return usuario;
    }
}
